# Docker Support Scripts

## Content

This repository contains some bash scripts that I created and that were useful to manage servers with docker containers, as well as speed up and automate some routine tasks and create an interactive menu to facilitate the interaction with Docker for people without much experience.

Scripts:

- check-container-status: This script check connectivity of expected Docker network for a defined number of containers and send email if the numbers of containers up are not the expected.

- docker-install-centos7: This script install docker and add user to docker group on Centos7.

- easy-docker: Script to interact with Docker easier and faster, this script speed up some routines as list all the docker content or delete all the docker content, also makes it easier to interact with docker thanks to its terminal interface. This script support command and interface mode.

- tar-backup: This script create backup from work directory to backup directory, also you can backup specific elements. Additionally this script rotate the backups files on backup directory with frequency specified and send this backups to email.


## License

Like all my projects this program is licensed under GPL 3.0 License. You can do anything that you want with my work except change license type, I will be grateful if you mention me.

## Misc

Author: m0rfeo

Open to suggestions :)
