#!/bin/bash

# GNU GENERAL PUBLIC LICENSE - Version 3, 29 June 2007
# Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
#
# Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.
# Author: m0rfeo
#
# This script install docker and add user to docker group on Centos7

# Function to remove old docker packages
removeOldVersion() {
sudo yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
};

# Function to add user to docker group
addUser() {
	echo -n "Do you want to add any user to Docker group?(y/n): "
	read add
	if [ $add = "y" ]
	then
		echo -n "What user you want to add (have to exist): "
		read user
		sudo usermod $user -aG docker
		echo "$user added to docker group, if still can't use Docker as non-root type on terminal su - $user"
		echo "Exiting..."
		exit
	elif [ $add = "n" ]
	then
		echo "Anything more to do..."
		echo "Exiting..."
		exit
	else
		echo "Wrong choice (y/n)"
		addUser
	fi
};

# Function to install docker packages on computer
installDocker() {
echo -n "You want to install docker on your computer?(y/n): "
read installDocker
if [ $installDocker = "y" ]
then
	sudo yum install -y yum-utils
	sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
	sudo yum update -y
	sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin
	echo "Docker installed!"
	addUser
elif [ $installDocker = "n" ]
then
	echo "Exiting..."
	exit
else
	echo "Wrong choice (y/n)"
	installDocker
fi
};

# Main function (here are used all functions)
main() {
echo "----- WELCOME TO DOCKER INSTALLER SCRIPT FOR CENTOS7 -----"
echo -n "Have you old version of Docker on your computer?(y/n): "
read old
if [ $old = "y" ]
then
	removeOldVersion
	installDocker
elif [ $old = "n" ]
then
	installDocker
else
	echo "Wrong choice (y/n)"
	main
fi
};

# Just have to exec main function
main






